<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0, width=device-width">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans">
    <link rel="stylesheet" href="fonts/simple-line-icons.css">
    <link rel="stylesheet" href="fonts/ionicons.min.css">
    <link rel="stylesheet" href="css/lightslider.min.css">
    <link rel="stylesheet" href="css/main.css">
    <title>111111</title>
</head>
<body>
<header>
    <div class="header_area">
        <div class="header_search_active">
            <div class="sidebar_search_icon">
                <button class="search_close">
                    <span class="icon-close"></span>
                </button>
            </div>
            <div class="sidebar_search_input">
                <form>
                    <div class="form_search">
                        <input id="search" class="input_text" value placeholder="Search entire store here ..."
                               type="search">
                        <button class="search_btn" type="button">
                            <a href="index.php">
                                <i class="icon-magnifier"></i>
                            </a>
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <div class="header_top">
            <div class="container">
                <div class="align_helper">
                    <div class="welcome_message">
                        <p>Default welcome msg!</p>
                    </div>
                    <div class="currancy_language">
                        <ul>
                            <li class="header_dropdown">
                                <a href="#">
                                    $ Dollar (US)
                                    <i class="ion-ios-arrow-down"></i>
                                </a>
                                <ul class="open_dropdown_menu">
                                    <li>
                                        <a href="#">Euro &#8364;</a>
                                    </li>
                                    <li>
                                        <a href="#"> USD $</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="header_dropdown">
                                <a href="#">
                                    <img src="images/la-1.jpg" alt="">
                                    English
                                    <i class="ion-ios-arrow-down"></i>
                                </a>
                                <ul class="open_dropdown_menu">
                                    <li>
                                        <a href="#">
                                            <img src="images/la-1.jpg" alt="">
                                            English
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="images/la-2.jpg" alt="">
                                            Fran?ais
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="header_bottom">
            <div class="container">
                <div class="align_helper">
                    <div class="header_logo">
                        <div class="logo">
                            <a href="index.php">
                                <img src="images/logo.jpg" alt="logo">
                            </a>
                        </div>
                    </div>
                    <div class="header_menu">
                        <div class="main_menu_area">
                            <nav class="main_navigation">
                                <ul>
                                    <li>
                                        <a href="index.php">Home</a>
                                        <ul class="sub_menu">
                                            <li><a href="index.php">Home Page 1</a></li>
                                            <li><a href="index.php">Home page 2</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="index.php">Pages</a>
                                        <ul class="mega_menu">
                                            <li>
                                                <a href="#">Column One</a>
                                                <ul>
                                                    <li><a href="#">Compare Page</a></li>
                                                    <li><a href="#">FAQ</a></li>
                                                    <li><a href="#">Login&Register</a></li>
                                                    <li><a href="#">My Account Page</a></li>
                                                </ul>
                                            </li>
                                            <li>
                                                <a href="#">Colunn Two</a>
                                                <ul>
                                                    <li><a href="#">Product Details 1</a></li>
                                                    <li><a href="#">Product Details 2</a></li>
                                                    <li><a href="#">Product Details 3</a></li>
                                                    <li><a href="#">Product Details 4</a></li>
                                                </ul>
                                            </li>
                                            <li>
                                                <a href="#">Column Three</a>
                                                <ul>
                                                    <li><a href="#">Error 404</a></li>
                                                    <li><a href="#">Cart Page</a></li>
                                                    <li><a href="#">Checkout Page</a></li>
                                                    <li><a href="#">Wishlist Page</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="index.php">Shop</a>
                                        <ul class="sub_menu">
                                            <li><a href="#">Shop Left</a></li>
                                            <li><a href="#">Shop Right</a></li>
                                            <li><a href="#">Shop List Left</a></li>
                                            <li><a href="#">Shop List Right</a></li>
                                            <li><a href="#">Shop Full Width</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="index.php">Blog</a>
                                        <ul class="sub_menu">
                                            <li><a href="#">Blog Page</a></li>
                                            <li><a href="#">Blog Right</a></li>
                                            <li><a href="#">Blog Full Width</a></li>
                                            <li><a href="#">Blog Details</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="index.php">About</a></li>
                                    <li><a href="index.php">Contact Us</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <div class="header_custom">
                        <div class="header_right_block_inner">
                            <div class="header_search">
                                <a href="#" class="trigger_search">
                                    <i class="icon-magnifier"></i>
                                </a>
                            </div>
                            <div class="header_user">
                                <a href="#">
                                    <i class="icon-user"></i>
                                </a>
                            </div>
                            <div class="header_basket">
                                <a href="#">
                                    <i class="icon-handbag"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<div>SLIDER HERE</div>
<div class="about_product">
    <div class="container">
        <div class="align_helper">
            <div class="product_about_img">
                <div class="about_img">
                    <a href="index.php">
                        <img src="images/about_img/about-1.jpg" alt="">
                    </a>
                    <span class="text_left">Welcome To Furniture</span>
                </div>
            </div>
            <div class="product_about_text">
                <div class="about_content">
                    <h3>Dining Table Set</h3>

                    <div class="price_box">
                        <span class="old_price">$330.00</span> -<span class="new_price"> $230.00</span>
                    </div>
                    <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eius tem incidid ut labore et
                        dolore mag aliqua.
                        Ut enim ad minim veniam, quis nostrud exercitati ullamco laboris nisi ut aliquip ex ea commodo
                        consequ.
                        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
                        pariatur.
                        Excepte sint occaecat cupidatat non proident, sunt in culpa qui.
                    </p>
                    <button class="about_content_btn">SHOP NOW</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="product_area">
    <div class="container">
        <div class="align_helper">
            <div class="product_area_title">
                <div class="product_title">
                    <h2>New Arrivals</h2>

                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit</p>
                </div>
            </div>
        </div>
        <div class="product_grid">
            <div class="align_helper">
                <div class="product_container">
                    <div class="product">
                        <div class="product_image">
                            <a href="#">
                                <img src="images/product/1.jpg" alt="">
                            </a>

                            <div class="product_action">
                                <a href="#" class="wishlist">
                                    <i class="icon-heart"></i>
                                </a>
                                <a href="#" class="add_to_cart">
                                    <i class="icon-handbag"></i>
                                </a>
                                <a href="#" class="quick_view">
                                    <i class="icon-shuffle"></i>
                                </a>
                            </div>
                        </div>
                        <div class="product_content">
                            <h3><a href="index.php">Product Name 001</a></h3>

                            <div class="product_price">
                                <span class="product_old_price">140.00</span>
                                <span class="product_new_price">120.00</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="product_container">
                    <div class="product">
                        <div class="product_image">
                            <a href="#">
                                <img src="images/product/2.jpg" alt="">
                            </a>

                            <div class="product_action">
                                <a href="#" class="wishlist">
                                    <i class="icon-heart"></i>
                                </a>
                                <a href="#" class="add_to_cart">
                                    <i class="icon-handbag"></i>
                                </a>
                                <a href="#" class="quick_view">
                                    <i class="icon-shuffle"></i>
                                </a>
                            </div>
                        </div>
                        <div class="product_content">
                            <h3><a href="index.php">Product Name 002</a></h3>

                            <div class="product_price">
                                <span class="product_old_price"></span>
                                <span class="product_new_price">120.00</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="product_container">
                    <div class="product">
                        <div class="product_image">
                            <a href="#">
                                <img src="images/product/3.jpg" alt="">
                            </a>

                            <div class="product_action">
                                <a href="#" class="wishlist">
                                    <i class="icon-heart"></i>
                                </a>
                                <a href="#" class="add_to_cart">
                                    <i class="icon-handbag"></i>
                                </a>
                                <a href="#" class="quick_view">
                                    <i class="icon-shuffle"></i>
                                </a>
                            </div>
                        </div>
                        <div class="product_content">
                            <h3><a href="index.php">Product Name 003</a></h3>

                            <div class="product_price">
                                <span class="product_old_price">230.00</span>
                                <span class="product_new_price">210.00</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="product_container">
                    <div class="product">
                        <div class="product_image">
                            <a href="#">
                                <img src="images/product/4.jpg" alt="">
                            </a>

                            <div class="product_action">
                                <a href="#" class="wishlist">
                                    <i class="icon-heart"></i>
                                </a>
                                <a href="#" class="add_to_cart">
                                    <i class="icon-handbag"></i>
                                </a>
                                <a href="#" class="quick_view">
                                    <i class="icon-shuffle"></i>
                                </a>
                            </div>
                        </div>
                        <div class="product_content">
                            <h3><a href="index.php">Product Name 004</a></h3>

                            <div class="product_price">
                                <span class="product_old_price"></span>
                                <span class="product_new_price">120.00</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="product_container">
                    <div class="product">
                        <div class="product_image">
                            <a href="#">
                                <img src="images/product/5.jpg" alt="">
                            </a>

                            <div class="product_action">
                                <a href="#" class="wishlist">
                                    <i class="icon-heart"></i>
                                </a>
                                <a href="#" class="add_to_cart">
                                    <i class="icon-handbag"></i>
                                </a>
                                <a href="#" class="quick_view">
                                    <i class="icon-shuffle"></i>
                                </a>
                            </div>
                        </div>
                        <div class="product_content">
                            <h3><a href="index.php">Product Name 005</a></h3>

                            <div class="product_price">
                                <span class="product_old_price">180.00</span>
                                <span class="product_new_price">150.00</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="product_container">
                    <div class="product">
                        <div class="product_image">
                            <a href="#">
                                <img src="images/product/6.jpg" alt="">
                            </a>

                            <div class="product_action">
                                <a href="#" class="wishlist">
                                    <i class="icon-heart"></i>
                                </a>
                                <a href="#" class="add_to_cart">
                                    <i class="icon-handbag"></i>
                                </a>
                                <a href="#" class="quick_view">
                                    <i class="icon-shuffle"></i>
                                </a>
                            </div>
                        </div>
                        <div class="product_content">
                            <h3><a href="index.php">Product Name 006</a></h3>

                            <div class="product_price">
                                <span class="product_old_price"></span>
                                <span class="product_new_price">130.00</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="product_container">
                    <div class="product">
                        <div class="product_image">
                            <a href="#">
                                <img src="images/product/7.jpg" alt="">
                            </a>

                            <div class="product_action">
                                <a href="#" class="wishlist">
                                    <i class="icon-heart"></i>
                                </a>
                                <a href="#" class="add_to_cart">
                                    <i class="icon-handbag"></i>
                                </a>
                                <a href="#" class="quick_view">
                                    <i class="icon-shuffle"></i>
                                </a>
                            </div>
                        </div>
                        <div class="product_content">
                            <h3><a href="#">Product Name 007</a></h3>

                            <div class="product_price">
                                <span class="product_old_price">250.00</span>
                                <span class="product_new_price">230.00</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="product_container">
                    <div class="product">
                        <div class="product_image">
                            <a href="#">
                                <img src="images/product/8.jpg" alt="">
                            </a>

                            <div class="product_action">
                                <a href="#" class="wishlist">
                                    <i class="icon-heart"></i>
                                </a>
                                <a href="#" class="add_to_cart">
                                    <i class="icon-handbag"></i>
                                </a>
                                <a href="#" class="quick_view">
                                    <i class="icon-shuffle"></i>
                                </a>
                            </div>
                        </div>
                        <div class="product_content">
                            <h3><a href="index.php">Product Name 008</a></h3>

                            <div class="product_price">
                                <span class="product_old_price"></span>
                                <span class="product_new_price">120.00</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="banner_area">
    <div class="container">
        <div class="align_helper">
            <div class="banner_info">
                <div class="information">
                    <h2>Contrary to popular belief is not simply rand.</h2>

                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit <br> sed do eiusmod tempor incid</p>
                    <a href="" class="information_btn">MORE PRODUCNTS</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="product_area">
    <div class="container">
        <div class="align_helper">
            <div class="product_area_title">
                <div class="product_title">
                    <h2>Featured Products</h2>

                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit</p>
                </div>
            </div>
        </div>
        <div class="product_grid">
            <div class="align_helper">
                <div class="product_container">
                    <div class="product">
                        <div class="product_image">
                            <a href="#">
                                <img src="images/product/9.jpg" alt="">
                            </a>

                            <div class="product_action">
                                <a href="#" class="wishlist">
                                    <i class="icon-heart"></i>
                                </a>
                                <a href="#" class="add_to_cart">
                                    <i class="icon-handbag"></i>
                                </a>
                                <a href="#" class="quick_view">
                                    <i class="icon-shuffle"></i>
                                </a>
                            </div>
                        </div>
                        <div class="product_content">
                            <h3><a href="index.php">Product Name 001</a></h3>

                            <div class="product_price">
                                <span class="product_old_price">150.00</span>
                                <span class="product_new_price">125.00</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="product_container">
                    <div class="product">
                        <div class="product_image">
                            <a href="#">
                                <img src="images/product/10.jpg" alt="">
                            </a>

                            <div class="product_action">
                                <a href="#" class="wishlist">
                                    <i class="icon-heart"></i>
                                </a>
                                <a href="#" class="add_to_cart">
                                    <i class="icon-handbag"></i>
                                </a>
                                <a href="#" class="quick_view">
                                    <i class="icon-shuffle"></i>
                                </a>
                            </div>
                        </div>
                        <div class="product_content">
                            <h3><a href="index.php">Product Name 002</a></h3>

                            <div class="product_price">
                                <span class="product_old_price">144.00</span>
                                <span class="product_new_price">124.00</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="product_container">
                    <div class="product">
                        <div class="product_image">
                            <a href="#">
                                <img src="images/product/11.jpg" alt="">
                            </a>

                            <div class="product_action">
                                <a href="#" class="wishlist">
                                    <i class="icon-heart"></i>
                                </a>
                                <a href="#" class="add_to_cart">
                                    <i class="icon-handbag"></i>
                                </a>
                                <a href="#" class="quick_view">
                                    <i class="icon-shuffle"></i>
                                </a>
                            </div>
                        </div>
                        <div class="product_content">
                            <h3><a href="index.php">Product Name 003</a></h3>

                            <div class="product_price">
                                <span class="product_old_price">130.00</span>
                                <span class="product_new_price">120.00</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="product_container">
                    <div class="product">
                        <div class="product_image">
                            <a href="#">
                                <img src="images/product/12.jpg" alt="">
                            </a>

                            <div class="product_action">
                                <a href="#" class="wishlist">
                                    <i class="icon-heart"></i>
                                </a>
                                <a href="#" class="add_to_cart">
                                    <i class="icon-handbag"></i>
                                </a>
                                <a href="#" class="quick_view">
                                    <i class="icon-shuffle"></i>
                                </a>
                            </div>
                        </div>
                        <div class="product_content">
                            <h3><a href="index.php">Product Name 004</a></h3>

                            <div class="product_price">
                                <span class="product_old_price"></span>
                                <span class="product_new_price">120.00</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="sale_area">
    <div class="container">
        <div class="align_helper">
            <div class="sale_banner">
                <div class="sale_banner_inner">
                    <div class="banner_bg_left">
                        <a href="">
                            <img src="images/banner/1.jpg" alt="">
                        </a>
                    </div>
                    <div class="banner_content">
                        <p>30% Off</p>
                        <h3>Chair Collection</h3>
                        <a href="#" class="sale_btn">SHOP NOW</a>
                    </div>
                </div>
            </div>
            <div class="sale_banner">
                <div class="sale_banner_inner">
                    <div class="banner_bg_right">
                        <a href="">
                            <img src="images/banner/2.jpg" alt="">
                        </a>
                    </div>
                    <div class="banner_content">
                        <p>30% Off</p>
                        <h3>Chair Collection</h3>
                        <a href="#" class="sale_btn">SHOP NOW</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="slider_with_comments">
    <div class="container">
        <div class="row">
            <div class="comments">
                <div class="slider_for_comments">

                </div>
            </div>
        </div>
    </div>
</div>
<div class="follow_area">
    <div class="container">
        <div class="align_helper">
            <div class="follow_instagramm">
                <div class="follow_instagramm_inner">
                    <div class="instagramm_title">
                        <h3>Follow us on Instagram <a href="#">@furniture</a></h3>
                    </div>
                    <div class="instagramm_photo">
                        <div class="instagramm_slider">
                            <div class="slider_instagramm">
                                <ul id="instagramm">
                                    <li>
                                        <a href="#">
                                            <img src="images/instagramm/1.jpg" alt="">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="images/instagramm/2.jpg" alt="">
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="subscribe_area">
                <div class="subscribe">
                    <div class="subscribe_title">
                       <h3>Subscribe</h3>
                        <p>Lorem ipsum dolor sit amet consectetur elit.</p>
                    </div>
                    <div class="subscribe_content">
                        <input class="input_field" type="email" placeholder="your mail address">
                        <button class="subscribe_btn">SUBSCRIBE</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="slider_brand_area">
    <div class="container">
        <div class="align_helper">
            <div class="slider_brand">
                <ul id="lightSlider">
                    <li>
                        <img src="images/brand/1.jpg" alt="">
                    </li>
                    <li>
                        <img src="images/brand/2.jpg" alt="">
                    </li>
                    <li>
                        <img src="images/brand/3.jpg" alt="">
                    </li>
                    <li>
                        <img src="images/brand/4.jpg" alt="">
                    </li>
                    <li>
                        <img src="images/brand/5.jpg" alt="">
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<footer>
    <div class="footer_area">
        <div class="footer_top">
            <div class="container">
                <div class="align_helper">
                    <div class="my_account">
                        <div class="account_information">
                            <div class="account_title">
                                <h3>My Account</h3>
                            </div>
                            <ul class="account_info">
                                <li>
                                    <i class="icon-location-pin"></i>
                                    184 Main Rd E, St Albans VIC 3021, Australia
                                </li>
                                <li>
                                    <i class="icon-envelope-letter"></i>
                                    Mill Us : <a href="#">yourmail@gmail.com</a>
                                </li>
                                <li>
                                    <i class="icon-phone"></i>
                                    Phone: + 00 254 254565 54
                                </li>
                            </ul>
                            <div class="payment">
                                <img src="images/payment/1.png" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="categories">
                        <div class="categories_list">
                            <div class="categories_title">
                                <h3>Categories</h3>
                            </div>
                            <ul class="list">
                                <li>
                                    <a href="#">Ecommerce</a>
                                </li>
                                <li>
                                    <a href="#">Shopify</a>
                                </li>
                                <li>
                                    <a href="#">Prestashop</a>
                                </li>
                                <li>
                                    <a href="#">Opencart</a>
                                </li>
                                <li>
                                    <a href="#">Magento</a>
                                </li>
                                <li>
                                    <a href="#">Jigoshop</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="footer_information">
                        <div class="information_list">
                            <div class="information_title">
                                <h3>Information</h3>
                            </div>
                            <ul class="list">
                                <li>
                                    <a href="#">Home</a>
                                </li>
                                <li>
                                    <a href="#">About Us</a>
                                </li>
                                <li>
                                    <a href="#">Contact Us</a>
                                </li>
                                <li>
                                    <a href="#">Returns & Exchanges</a>
                                </li>
                                <li>
                                    <a href="#">Shipping & Delivery</a>
                                </li>
                                <li>
                                    <a href="#">Privacy Policy</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="quick_links">
                        <div class="quick_links_list">
                            <div class="quick_links_title">
                                <h3>Quick Links</h3>
                            </div>
                            <ul class="list">
                                <li>
                                    <a href="#">Store Location</a>
                                </li>
                                <li>
                                    <a href="#">My Account</a>
                                </li>
                                <li>
                                    <a href="#">Order Tracking</a>
                                </li>
                                <li>
                                    <a href="#">Size Guide</a>
                                </li>
                                <li>
                                    <a href="#">Shopping Rates</a>
                                </li>
                                <li>
                                    <a href="#">Contact Us</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer_bottom">
            <div class="container">
                <div class="align_helper">
                    <div class="copyright_block">
                        <div class="copyright">
                            <p>Copyright&#169; All Right Reserved.</p>
                        </div>
                    </div>
                    <div class="social_block">
                        <div class="social">
                            <ul class="social_list">
                                <li>
                                    <a href="#">
                                        <i class="icon-social-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-social-tumblr"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-social-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon-social-instagram"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="JS/header.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="JS/lightslider.min.js"></script>
<script src="JS/slider_top_function.js"></script>
<script src="JS/slider_brand_function.js"></script>
</body>
</html>